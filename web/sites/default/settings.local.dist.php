<?php
$databases['default']['default'] = [
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USERNAME'),
  'password' => getenv('DB_PASSWORD'),
  'prefix' => getenv('DB_PREFIX'),
  'host' => getenv('DB_HOST'),
  'port' => getenv('DB_PORT'),
  'namespace' => 'Drupal\\mysql\\Driver\\Database\\mysql',
  'driver' => getenv('DB_DRIVER'),
  'autoload' => 'core/modules/mysql/src/Driver/Database/mysql/',
];

$settings['hash_salt'] = getenv('HASH_SALT');
$settings['trusted_host_patterns'] = [getenv('TRUSTED_HOSTS_PATTERNS')];
$settings['config_sync_directory'] = getenv('CONFIG_DIR');
$settings['file_private_path'] = getenv('PRIVATE_PATH');

//$config['system.logging']['error_level'] = 'verbose';
